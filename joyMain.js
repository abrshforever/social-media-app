let form=document.getElementById("form");
let input=document.getElementById("input");
let msg=document.getElementById("msg");
let posts=document.getElementById("posts")

form.addEventListener("submit",(e)=>{
    e.preventDefault();
    formValidation();

})

let formValidation=()=>{
    if(input.value===''){ 
        msg.innerHTML="You  post nothhing";
        
    }
    else{
        msg.innerHTML=''
        acceptData();

    }
}
let data={};
let acceptData=()=>{
    data["text"]=input.value;
    createPost();
    input.value=''
}
let createPost=()=>{
    posts.innerHTML+=
    ` <div>
        <p>${data.text}</p>
        <article class="icons">
            <span onClick="editPost(this)" id="edit">Edit</span>
            <span onClick="deletePost(this)" id="delete">Delete</span>
        </article>
     </div>
    `;

}
let deletePost=(e)=>{
    e.parentElement.parentElement.remove();

};
const editPost=(ev)=>{
    input.value=ev.parentElement.previousElementSibling.innerHTML;
    ev.parentElement.parentElement.remove();

}
// let saveData=()=>{
//     localStorage.setItem("savedData",input.innerHTML)
// }
// let showData=()=>{
//     input.innerHTML=localStorage.getItem("savedData");
// }
// showData()
